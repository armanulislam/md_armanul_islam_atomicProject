<?php
namespace App\bitm\seip137028\book;

class Book{
    public $id="";
    public $title="";
    public $conn;


    /**
     * @param string $data
     */
    public function prepare($data=""){
        if(array_key_exists("title",$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        //echo  $this;

    }

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","books") or die("Database connection failed");
    }

    public function store(){
        $query="INSERT INTO `books`.`book` (`title`) VALUES ('".$this->title."')";

        //echo $query;

        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }
        else {
            echo "Error";
        }
    }

    public function index(){
        $_allBook=array();
        $query="SELECT * FROM `book`";
        $result= mysqli_query($this->conn,$query);

        while($row=mysqli_fetch_object($result)){
            $_allBook[]=$row;
        }

        return $_allBook;


    }



}