<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-BOOK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
<body>

<div class="container">
    <h2>Create Book Title</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Book Title:</label>
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
