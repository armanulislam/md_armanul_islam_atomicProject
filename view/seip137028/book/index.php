<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\bitm\seip137028\book\Book;
use App\bitm\seip137028\Book\Utility;
use App\bitm\seip137028\Book\Message;

$book= new Book();
$allBook=$book->index();
//Utility::d($allBook);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All Book List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>
    <?php echo Message::message()?>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Book title</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allBook as $book){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $book-> id?></td>
                <td><?php echo $book->title?></td>
                <td><a href="view.php" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php" class="btn btn-danger" role="button">Delete</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>

</body>
</html>
