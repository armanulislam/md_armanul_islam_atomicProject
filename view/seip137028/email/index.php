<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\bitm\seip137028\email\Email;
use App\bitm\seip137028\email\Utility;
use App\bitm\seip137028\email\Message;

$book= new Email();
$allBook=$book->index();
//Utility::d($allBook);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
<body>

<div class="container">
    <h2>Email subscrib</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>
    <?php echo Message::message()?>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Email</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allBook as $book){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $book-> id?></td>
                <td><?php echo $book->email?></td>
                <td><a href="view.php" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php" class="btn btn-danger" role="button">Delete</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>

</body>
</html>
