<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email subscription</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
<body>

<div class="container">
    <h2>Email subscription</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter your email:</label>
            <input type="text" name="email" class="form-control" id="email" placeholder="Enter your email">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
